/*
 * MIT License
 *
 * Copyright (c) 2023 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package plugin

import (
	"fmt"
	"net/url"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/go-plugin"

	"gitlab.com/unboundsoftware/sloth/model"
	mp "gitlab.com/unboundsoftware/sloth/model/plugin"
)

func New(logger hclog.Logger) *Manager {
	return &Manager{Logger: logger}
}

// Manager loads and manages Sloth plugins.
//
// After creating a Manager value, call LoadPlugins with a directory path to
// discover and load plugins. At the end of the program call Close to kill and
// clean up all plugin processes.
type Manager struct {
	Logger        hclog.Logger
	Sources       []model.Source
	Sinks         []model.Sink
	Stores        []model.Store
	pluginClients []*plugin.Client
}

// LoadPlugins takes a directory path and assumes that all files within it
// are plugin binaries. It runs all these binaries in sub-processes,
// establishes RPC communication with the plugins, and registers them for
// the hooks they declare to support.
func (m *Manager) LoadPlugins(path string, args []string) error {
	m.Sources = []model.Source{}
	m.Sinks = []model.Sink{}

	binaries, err := plugin.Discover("*", path)
	if err != nil {
		return err
	}

	pluginMap := map[string]plugin.Plugin{
		"source": &mp.SourcePlugin{},
		"sink":   &mp.SinkPlugin{},
		"store":  &mp.StorePlugin{},
	}

	for _, bpath := range binaries {
		client := plugin.NewClient(&plugin.ClientConfig{
			HandshakeConfig: mp.Handshake,
			Plugins:         pluginMap,
			Cmd:             exec.Command(bpath),
			Logger:          m.Logger.With("plugin", filepath.Base(bpath)),
		})
		m.pluginClients = append(m.pluginClients, client)

		rpcClient, err := client.Client()
		if err != nil {
			return err
		}

		for k := range pluginMap {
			raw, err := rpcClient.Dispense(k)
			if err != nil {
				if strings.HasPrefix(err.Error(), "unknown plugin type:") {
					continue
				}
				return err
			}

			if err := m.addPlugin(k, raw.(model.Plugin), args); err != nil {
				return err
			}
		}
	}

	return nil
}

func (m *Manager) addPlugin(kind string, plugin model.Plugin, args []string) error {
	switch kind {
	case "source":
		if plugin.IsSource() {
			configured, err := plugin.Configure(args)
			if err != nil {
				return err
			}
			if configured {
				m.Sources = append(m.Sources, plugin.(model.Source))
			}
		}
	case "sink":
		if plugin.IsSink() {
			configured, err := plugin.Configure(args)
			if err != nil {
				return err
			}
			if configured {
				m.Sinks = append(m.Sinks, plugin.(model.Sink))
			}
		}
	case "store":
		if plugin.IsStore() {
			configured, err := plugin.Configure(args)
			if err != nil {
				return err
			}
			if configured {
				m.Stores = append(m.Stores, plugin.(model.Store))
			}
		}
	}

	return nil
}

func (m *Manager) Close() {
	for _, client := range m.pluginClients {
		client.Kill()
	}
}

func (m *Manager) Start() error {
	if len(m.Sources) == 0 || len(m.Sinks) == 0 {
		m.Logger.Warn("No sources and/or sinks found")
		return fmt.Errorf("no sources and/or sinks found")
	}
	if m.Stores == nil || len(m.Stores) != 1 {
		m.Logger.Warn(fmt.Sprintf("Exactly one store needs to be configured, found %d", len(m.Stores)))
		return fmt.Errorf("exactly one store needs to be configured, found %d", len(m.Stores))
	}

	quit := make(model.ChannelQuitter)
	for _, s := range m.Sinks {
		if err := s.Start(quit); err != nil {
			return err
		}
	}
	for _, s := range m.Sources {
		if err := s.Start(m, quit); err != nil {
			return err
		}
	}
	if err := m.Stores[0].Start(model.HandlerFunc(m.ToSink), quit); err != nil {
		return err
	}
	return <-quit
}

func (m *Manager) ToSink(request model.Request) error {
	parsed, err := url.Parse(request.Target)
	if err != nil {
		return err
	}
	scheme := parsed.Scheme
	found := false
	for _, sink := range m.Sinks {
		if sink.Accept(scheme) {
			found = true
			m.Logger.Info(fmt.Sprintf("publishing request to sink with scheme '%s' and target '%s%s'", scheme, parsed.Host, parsed.Path))
			if err := sink.Publish(request); err != nil {
				return err
			}
		}
	}
	if !found {
		return fmt.Errorf("no sink found for scheme: %s", scheme)
	}
	return nil
}

func (m *Manager) Stop() error {
	for _, s := range m.Sources {
		if err := s.Stop(); err != nil {
			return err
		}
	}
	for _, s := range m.Stores {
		if err := s.Stop(); err != nil {
			return err
		}
	}
	for _, s := range m.Sinks {
		if err := s.Stop(); err != nil {
			return err
		}
	}
	return nil
}

func (m *Manager) Store(request model.Request) error {
	m.Logger.Info(fmt.Sprintf("delaying request to %s until %s", request.Target, request.DelayedUntil.Format(time.RFC3339)))
	return m.Stores[0].Add(request)
}

var _ model.Storer = &Manager{}
