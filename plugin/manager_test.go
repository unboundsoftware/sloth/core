/*
 * MIT License
 *
 * Copyright (c) 2023 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package plugin

import (
	"bytes"
	"errors"
	"fmt"
	"reflect"
	"strings"
	"testing"
	"time"

	"github.com/hashicorp/go-hclog"
	"github.com/sanity-io/litter"
	"github.com/stretchr/testify/assert"
	"gitlab.com/unboundsoftware/sloth/model"
)

func TestManager_Start(t *testing.T) {
	type fields struct {
		sources []model.Source
		sinks   []model.Sink
		stores  []model.Store
	}
	tests := []struct {
		name       string
		fields     fields
		wantErr    bool
		wantLogged []string
	}{
		{
			name: "no sources",
			fields: fields{
				sinks:   []model.Sink{},
				sources: []model.Source{},
			},
			wantErr:    true,
			wantLogged: []string{"[WARN]  No sources and/or sinks found\n"},
		},
		{
			name: "no sinks",
			fields: fields{
				sinks: []model.Sink{},
				sources: []model.Source{&MockSource{start: func(handler model.Storer, quitter model.Quitter) error {
					return errors.New("error")
				}}},
			},
			wantErr:    true,
			wantLogged: []string{"[WARN]  No sources and/or sinks found\n"},
		},
		{
			name: "no store",
			fields: fields{
				sinks: []model.Sink{&MockSink{start: func(quitter model.Quitter) error {
					return errors.New("error")
				}}},
				sources: []model.Source{&MockSource{start: func(handler model.Storer, quitter model.Quitter) error {
					return errors.New("error")
				}}},
			},
			wantErr:    true,
			wantLogged: []string{"[WARN]  Exactly one store needs to be configured, found 0\n"},
		},
		{
			name: "multiple stores",
			fields: fields{
				sinks: []model.Sink{&MockSink{start: func(quitter model.Quitter) error {
					return errors.New("error")
				}}},
				sources: []model.Source{&MockSource{start: func(handler model.Storer, quitter model.Quitter) error {
					return errors.New("error")
				}}},
				stores: []model.Store{&MockStore{start: func(handler model.Handler, quitter model.Quitter) error {
					return errors.New("error")
				}}, &MockStore{start: func(handler model.Handler, quitter model.Quitter) error {
					return errors.New("error")
				}}},
			},
			wantErr:    true,
			wantLogged: []string{"[WARN]  Exactly one store needs to be configured, found 2\n"},
		},
		{
			name: "start sink error",
			fields: fields{
				sinks: []model.Sink{&MockSink{start: func(quitter model.Quitter) error {
					return errors.New("error")
				}}},
				sources: []model.Source{&MockSource{start: func(handler model.Storer, quitter model.Quitter) error {
					return errors.New("error")
				}}},
				stores: []model.Store{&MockStore{start: func(handler model.Handler, quitter model.Quitter) error {
					return errors.New("error")
				}}},
			},
			wantErr: true,
		},
		{
			name: "start source error",
			fields: fields{
				sinks: []model.Sink{&MockSink{start: func(quitter model.Quitter) error {
					return nil
				}}},
				sources: []model.Source{&MockSource{start: func(handler model.Storer, quitter model.Quitter) error {
					return errors.New("error")
				}}},
				stores: []model.Store{&MockStore{start: func(handler model.Handler, quitter model.Quitter) error {
					return errors.New("error")
				}}},
			},
			wantErr: true,
		},
		{
			name: "start store error",
			fields: fields{
				sinks: []model.Sink{&MockSink{start: func(quitter model.Quitter) error {
					return nil
				}}},
				sources: []model.Source{&MockSource{start: func(handler model.Storer, quitter model.Quitter) error {
					return nil
				}}},
				stores: []model.Store{&MockStore{start: func(handler model.Handler, quitter model.Quitter) error {
					return errors.New("error")
				}}},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			m := &Manager{
				Sources: tt.fields.sources,
				Sinks:   tt.fields.sinks,
				Stores:  tt.fields.stores,
				Logger:  logger,
			}
			if err := m.Start(); (err != nil) != tt.wantErr {
				t.Errorf("Start() error = %v, wantErr %v", err, tt.wantErr)
			}
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
		})
	}
}

func TestManager_ToSink(t *testing.T) {
	type fields struct {
		sinks []model.Sink
	}
	type args struct {
		request model.Request
	}
	tests := []struct {
		name       string
		fields     fields
		args       args
		wantErr    bool
		wantLogged []string
	}{
		{
			name:       "unparsable url",
			fields:     fields{},
			args:       args{request: model.Request{Target: "test://host:abc/path"}},
			wantErr:    true,
			wantLogged: nil,
		},
		{
			name:       "no matching sink",
			fields:     fields{},
			args:       args{request: model.Request{Target: "test://host/path"}},
			wantErr:    true,
			wantLogged: nil,
		},
		{
			name: "error publishing",
			fields: fields{
				sinks: []model.Sink{
					&MockSink{
						accept: func(s string) bool {
							if s != "test" {
								t.Errorf("toSink() got %s, want test", s)
							}
							return true
						},
						publish: func(m model.Request) error {
							want := model.Request{Target: "test://host/path"}
							if !reflect.DeepEqual(m, want) {
								t.Errorf("toSink() got %s, want %s", litter.Sdump(m), litter.Sdump(want))
							}
							return errors.New("error")
						},
					},
				},
			},
			args:       args{request: model.Request{Target: "test://host/path"}},
			wantErr:    true,
			wantLogged: []string{"[INFO]  publishing request to sink with scheme 'test' and target 'host/path'\n"},
		},
		{
			name: "success",
			fields: fields{
				sinks: []model.Sink{
					&MockSink{
						accept: func(s string) bool {
							return true
						},
						publish: func(m model.Request) error {
							return nil
						},
					},
				},
			},
			args:       args{request: model.Request{Target: "test://host/path"}},
			wantErr:    false,
			wantLogged: []string{"[INFO]  publishing request to sink with scheme 'test' and target 'host/path'\n"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			m := &Manager{
				Sinks:  tt.fields.sinks,
				Logger: logger,
			}
			if err := m.ToSink(tt.args.request); (err != nil) != tt.wantErr {
				t.Errorf("toSink() error = %v, wantErr %v", err, tt.wantErr)
			}
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
		})
	}
}

func TestManager_Stop(t *testing.T) {
	type fields struct {
		sources []model.Source
		sinks   []model.Sink
		store   model.Store
	}
	tests := []struct {
		name       string
		fields     fields
		wantErr    bool
		wantLogged []string
	}{
		{
			name: "error stopping source",
			fields: fields{
				sources: []model.Source{
					&MockSource{stop: func() error {
						return errors.New("error")
					}},
				},
			},
			wantErr:    true,
			wantLogged: nil,
		},
		{
			name: "error stopping store",
			fields: fields{
				sources: []model.Source{
					&MockSource{stop: func() error {
						return nil
					}},
				},
				store: &MockStore{
					stop: func() error {
						return errors.New("error")
					},
				},
			},
			wantErr:    true,
			wantLogged: nil,
		},
		{
			name: "error stopping sink",
			fields: fields{
				sources: []model.Source{
					&MockSource{stop: func() error {
						return nil
					}},
				},
				store: &MockStore{
					stop: func() error {
						return nil
					},
				},
				sinks: []model.Sink{
					&MockSink{stop: func() error {
						return errors.New("error")
					}},
				},
			},
			wantErr:    true,
			wantLogged: nil,
		},
		{
			name: "success",
			fields: fields{
				sources: []model.Source{
					&MockSource{stop: func() error {
						return nil
					}},
				},
				store: &MockStore{
					stop: func() error {
						return nil
					},
				},
				sinks: []model.Sink{
					&MockSink{stop: func() error {
						return nil
					}},
				},
			},
			wantErr:    false,
			wantLogged: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			m := &Manager{
				Sources: tt.fields.sources,
				Sinks:   tt.fields.sinks,
				Stores:  []model.Store{tt.fields.store},
				Logger:  logger,
			}
			if err := m.Stop(); (err != nil) != tt.wantErr {
				t.Errorf("Stop() error = %v, wantErr %v", err, tt.wantErr)
			}
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
		})
	}
}

func TestManager_Store(t *testing.T) {
	type fields struct {
		store model.Store
	}
	type args struct {
		request model.Request
	}
	tests := []struct {
		name       string
		fields     fields
		args       args
		wantErr    bool
		wantLogged []string
	}{
		{
			name: "error adding request",
			fields: fields{store: &MockStore{
				add: func(request model.Request) error {
					want := model.Request{
						DelayedUntil: time.Date(2021, 1, 24, 15, 46, 12, 123456789, time.UTC),
						Target:       "test://host/path",
						Payload:      []byte("abc"),
					}
					if !reflect.DeepEqual(request, want) {
						t.Errorf("toSink() got %s, want %s", litter.Sdump(request), litter.Sdump(want))
					}
					return errors.New("error")
				},
			}},
			args: args{request: model.Request{
				DelayedUntil: time.Date(2021, 1, 24, 15, 46, 12, 123456789, time.UTC),
				Target:       "test://host/path",
				Payload:      []byte("abc"),
			}},
			wantErr:    true,
			wantLogged: []string{"[INFO]  delaying request to test://host/path until 2021-01-24T15:46:12Z\n"},
		},
		{
			name: "success",
			fields: fields{store: &MockStore{
				add: func(request model.Request) error {
					return nil
				},
			}},
			args: args{request: model.Request{
				DelayedUntil: time.Date(2021, 1, 24, 15, 46, 12, 123456789, time.UTC),
				Target:       "test://host/path",
				Payload:      []byte("abc"),
			}},
			wantErr:    false,
			wantLogged: []string{"[INFO]  delaying request to test://host/path until 2021-01-24T15:46:12Z\n"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			m := &Manager{
				Stores: []model.Store{tt.fields.store},
				Logger: logger,
			}
			if err := m.Store(tt.args.request); (err != nil) != tt.wantErr {
				t.Errorf("Store() error = %v, wantErr %v", err, tt.wantErr)
			}
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
		})
	}
}

func TestManager_addPlugin(t *testing.T) {
	type args struct {
		kind   string
		plugin model.Plugin
		args   []string
	}
	tests := []struct {
		name        string
		args        args
		wantErr     assert.ErrorAssertionFunc
		wantSources int
		wantSinks   int
		wantStores  int
	}{
		{
			name: "kind source - SinkPlugin",
			args: args{
				kind:   "source",
				plugin: &MockSink{},
				args:   nil,
			},
			wantErr:     assert.NoError,
			wantSources: 0,
			wantSinks:   0,
			wantStores:  0,
		},
		{
			name: "kind sink - StorePlugin",
			args: args{
				kind:   "sink",
				plugin: &MockStore{},
				args:   nil,
			},
			wantErr:     assert.NoError,
			wantSources: 0,
			wantSinks:   0,
			wantStores:  0,
		},
		{
			name: "kind store - SourcePlugin",
			args: args{
				kind:   "store",
				plugin: &MockSource{},
				args:   nil,
			},
			wantErr:     assert.NoError,
			wantSources: 0,
			wantSinks:   0,
			wantStores:  0,
		},
		{
			name: "SourcePlugin - Config error",
			args: args{
				kind: "source",
				plugin: &MockSource{config: func(args []string) (bool, error) {
					return false, fmt.Errorf("config error")
				}},
				args: nil,
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "config error")
			},
			wantSources: 0,
			wantSinks:   0,
			wantStores:  0,
		},
		{
			name: "SourcePlugin - Not Configured",
			args: args{
				kind: "source",
				plugin: &MockSource{config: func(args []string) (bool, error) {
					return false, nil
				}},
				args: nil,
			},
			wantErr:     assert.NoError,
			wantSources: 0,
			wantSinks:   0,
			wantStores:  0,
		},
		{
			name: "SourcePlugin - Configured",
			args: args{
				kind: "source",
				plugin: &MockSource{config: func(args []string) (bool, error) {
					return true, nil
				}},
				args: nil,
			},
			wantErr:     assert.NoError,
			wantSources: 1,
			wantSinks:   0,
			wantStores:  0,
		},
		{
			name: "SinkPlugin - Config error",
			args: args{
				kind: "sink",
				plugin: &MockSink{config: func(args []string) (bool, error) {
					return false, fmt.Errorf("config error")
				}},
				args: nil,
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "config error")
			},
			wantSources: 0,
			wantSinks:   0,
			wantStores:  0,
		},
		{
			name: "SinkPlugin - Not Configured",
			args: args{
				kind: "sink",
				plugin: &MockSink{config: func(args []string) (bool, error) {
					return false, nil
				}},
				args: nil,
			},
			wantErr:     assert.NoError,
			wantSources: 0,
			wantSinks:   0,
			wantStores:  0,
		},
		{
			name: "SinkPlugin - Configured",
			args: args{
				kind: "sink",
				plugin: &MockSink{config: func(args []string) (bool, error) {
					return true, nil
				}},
				args: nil,
			},
			wantErr:     assert.NoError,
			wantSources: 0,
			wantSinks:   1,
			wantStores:  0,
		},
		{
			name: "StorePlugin - Config error",
			args: args{
				kind: "store",
				plugin: &MockStore{config: func(args []string) (bool, error) {
					return false, fmt.Errorf("config error")
				}},
				args: nil,
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "config error")
			},
			wantSources: 0,
			wantSinks:   0,
			wantStores:  0,
		},
		{
			name: "StorePlugin - Not Configured",
			args: args{
				kind: "store",
				plugin: &MockStore{config: func(args []string) (bool, error) {
					return false, nil
				}},
				args: nil,
			},
			wantErr:     assert.NoError,
			wantSources: 0,
			wantSinks:   0,
			wantStores:  0,
		},
		{
			name: "StorePlugin - Configured",
			args: args{
				kind: "store",
				plugin: &MockStore{config: func(args []string) (bool, error) {
					return true, nil
				}},
				args: nil,
			},
			wantErr:     assert.NoError,
			wantSources: 0,
			wantSinks:   0,
			wantStores:  1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Manager{}
			tt.wantErr(t, m.addPlugin(tt.args.kind, tt.args.plugin, tt.args.args), fmt.Sprintf("addPlugin(%v, %v, %v)", tt.args.kind, tt.args.plugin, tt.args.args))
			assert.Equal(t, tt.wantSources, len(m.Sources))
			assert.Equal(t, tt.wantSinks, len(m.Sinks))
			assert.Equal(t, tt.wantStores, len(m.Stores))
		})
	}
}

type MockSource struct {
	start  func(handler model.Storer, quitter model.Quitter) error
	stop   func() error
	config func(args []string) (bool, error)
}

func (m *MockSource) IsSource() bool {
	return true
}

func (m *MockSource) IsSink() bool {
	return false
}

func (m *MockSource) IsStore() bool {
	return false
}

func (m *MockSource) Configure(args []string) (bool, error) {
	if m.config != nil {
		return m.config(args)
	}
	return false, nil
}

func (m *MockSource) Start(storer model.Storer, quitter model.Quitter) error {
	if m.start != nil {
		return m.start(storer, quitter)
	}
	return errors.New("error")
}

func (m *MockSource) Stop() error {
	if m.stop != nil {
		return m.stop()
	}
	return errors.New("error")
}

var _ model.Source = &MockSource{}

type MockSink struct {
	start   func(quitter model.Quitter) error
	stop    func() error
	publish func(model.Request) error
	accept  func(string) bool
	config  func(args []string) (bool, error)
}

func (m *MockSink) IsSource() bool {
	return false
}

func (m *MockSink) IsSink() bool {
	return true
}

func (m *MockSink) IsStore() bool {
	return false
}

func (m *MockSink) Configure(args []string) (bool, error) {
	if m.config != nil {
		return m.config(args)
	}
	return false, nil
}

func (m *MockSink) Start(quitter model.Quitter) error {
	if m.start != nil {
		return m.start(quitter)
	}
	return errors.New("error")
}

func (m *MockSink) Publish(request model.Request) error {
	if m.publish == nil {
		return nil
	}
	return m.publish(request)
}

func (m *MockSink) Stop() error {
	if m.stop != nil {
		return m.stop()
	}
	return nil
}

func (m *MockSink) Accept(scheme string) bool {
	if m.accept == nil {
		return false
	}
	return m.accept(scheme)
}

var _ model.Sink = &MockSink{}

type MockStore struct {
	start  func(handler model.Handler, quitter model.Quitter) error
	stop   func() error
	add    func(request model.Request) error
	config func(args []string) (bool, error)
}

func (m *MockStore) IsSource() bool {
	return false
}

func (m *MockStore) IsSink() bool {
	return false
}

func (m *MockStore) IsStore() bool {
	return true
}

func (m *MockStore) Configure(args []string) (bool, error) {
	if m.config != nil {
		return m.config(args)
	}
	return false, nil
}

func (m *MockStore) Start(handler model.Handler, quitter model.Quitter) error {
	if m.start != nil {
		return m.start(handler, quitter)
	}
	return errors.New("error")
}

func (m *MockStore) Stop() error {
	if m.stop != nil {
		return m.stop()
	}
	return nil
}

func (m *MockStore) Add(request model.Request) error {
	if m.add != nil {
		return m.add(request)
	}
	return nil
}

var _ model.Store = &MockStore{}
