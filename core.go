/*
 * MIT License
 *
 * Copyright (c) 2023 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package core

import (
	"os"
	"os/signal"
	"syscall"

	"github.com/hashicorp/go-hclog"

	"gitlab.com/unboundsoftware/sloth/core/plugin"
)

func Start(args []string, logger hclog.Logger) error {
	var manager = plugin.New(logger)
	err := manager.LoadPlugins("./plugins", args)
	if err != nil {
		return err
	}
	defer manager.Close()

	ic := make(chan os.Signal, 1)
	ec := make(chan error, 1)

	go func() { ec <- manager.Start() }()
	defer func() {
		logger.Info("stopping core")
		if err := manager.Stop(); err != nil {
			logger.Error("error stopping", "error", err)
		}
	}()

	logger.Info("started")
	signal.Notify(ic, os.Interrupt, syscall.SIGTERM)

	select {
	case <-ic:
		logger.Info("signal received, exiting")
		return nil
	case err := <-ec:
		return err
	}
}
